﻿using System;

namespace rest.Models
{
    public class User
    {
        public int Id { get; set; }
        public int Iin { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
    }
}