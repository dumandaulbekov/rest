﻿using System.Collections.Generic;
using rest.Models;

namespace rest.DataAccess
{
    public interface IUserRepository
    {
        void Create(User user);
        void Update(User user);
        void Delete(int iin);
        User GetByIin(int iin);
        List<User> GetAll();
    }
}