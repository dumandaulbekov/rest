﻿using System.Security.Cryptography.X509Certificates;
using Microsoft.EntityFrameworkCore;
using rest.Models;

namespace rest.DataAccess
{
    public class Context : DbContext
    {
        public Context(DbContextOptions<Context> options) : base(options) { }

        public DbSet<User> Users { get; set; }
    }
}