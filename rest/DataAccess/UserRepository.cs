﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using rest.Models;

namespace rest.DataAccess
{
    public class UserRepository : IUserRepository
    {
        private readonly Context _context;

        public UserRepository(Context context)
        {
            _context = context;
        }

        public void Create(User user)
        {
            CheckIsNullOrEmpty(user.FirstName);
            CheckIsNullOrEmpty(user.LastName);

            CheckInvalidCharacters(user.FirstName);
            CheckInvalidCharacters(user.LastName);

            _context.Users.Add(user);
            _context.SaveChanges();
        }

        public void Update(User user)
        {
            var selectUser = GetByIin(user.Iin);

            CheckIsNullOrEmpty(user.FirstName);
            CheckIsNullOrEmpty(user.LastName);

            CheckInvalidCharacters(user.FirstName);
            CheckInvalidCharacters(user.LastName);

            selectUser.FirstName =  user.FirstName.Trim();
            selectUser.LastName = user.LastName.Trim();
            selectUser.BirthDate = user.BirthDate;

            _context.Users.Update(selectUser);
            _context.SaveChanges();
        }

        public void Delete(int iin)
        {
            var user = GetByIin(iin);

            _context.Users.Remove(user);
            _context.SaveChanges();
        }

        public User GetByIin(int iin)
        {
            var user = _context.Users.FirstOrDefault(c => c.Iin == iin);

            if (user == null) throw new Exception("Not found this user");

            return user;

        }

        public List<User> GetAll()
        {
            return _context.Users.ToList();
        }

        public void CheckInvalidCharacters(string value)
        {
            var result = Regex.IsMatch(value, @"[^a-zA-z\d_]");

            if (result)
            {
                throw new Exception($"not be invalid characters #@$...");
            }
        }

        public void CheckIsNullOrEmpty(string value)
        {
            var result = string.IsNullOrEmpty(value);

            if (result)
            {
                throw new Exception("Field FirstName not be empty");
            }
        }
    }
}