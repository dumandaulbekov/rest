﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using rest.DataAccess;
using rest.Models;

namespace rest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UsersController : ControllerBase
    {
        private readonly IUserRepository _userRepository;

        public UsersController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        [HttpGet]
        public ActionResult<List<User>> GetAllUser()
        {
            return Ok(_userRepository.GetAll());
        }

        [HttpGet("{iin}")]
        public ActionResult<User> GetByUserIin(int iin)
        {
            return Ok(_userRepository.GetByIin(iin));
        }

        [HttpPost]
        public ActionResult CreateUser([FromBody] User user)
        {
            _userRepository.Create(user);

            return Ok();
        }

        [HttpPut]
        public ActionResult UpdateUser([FromBody] User user)
        {
            _userRepository.Update(user);

            return Ok();
        }

        [HttpDelete("{iin}")]
        public ActionResult DeleteUser(int iin)
        {
            _userRepository.Delete(iin);

            return Ok();
        }
    }
}